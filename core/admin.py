from django.contrib import admin
from .models import Movie


@admin.register(Movie)
class MovieRepr(admin.ModelAdmin):

    fields = ("title",
              "plot",
              "year",
              "rating",
              "runtime",
              "website")
