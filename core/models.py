from django.db import models
from django.contrib.auth import get_user_model


class Movie(models.Model):
    NOT_RATED = 0
    RATED_G = 1
    RATED_PG = 2
    RATED_R = 3
    RATINGS = (
        (NOT_RATED, "NR - Not rated"),
        (RATED_G, "G - General Audiences"),
        (RATED_PG, "PG - Parental Guidance Suggested"),
        (RATED_R, "R - Restricted")
    )

    title = models.CharField(max_length=140)
    plot = models.TextField()
    year = models.PositiveIntegerField()
    rating = models.IntegerField(choices=RATINGS, default=NOT_RATED)
    runtime = models.PositiveIntegerField()
    website = models.URLField(blank=True)

    def __str__(self):
        return "{0} ({1})".format(self.title, self.year)

    class Meta:
        ordering = ("-year", "title")


class VoteManager(models.Manager):

    @staticmethod
    def get_vote_or_unsaved_blank_vote(movie, user):
        try:
            return Vote.objects.get(movie=movie,
                                    user=user)
        except Vote.DoesNotExist:
            return Vote(movie=movie,
                        user=user)


class Vote(models.Model):
    UP = 1
    DOWN = -1
    VALUE_CHOICES = ((UP, "UP"), (DOWN, "DOWN"))
    value = models.SmallIntegerField(choices=VALUE_CHOICES)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    voted_on = models.DateTimeField(auto_now=True)

    objects = VoteManager()

    class Meta:
        unique_together = ("user", "movie")

