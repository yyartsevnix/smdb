# Generated by Django 2.0 on 2019-05-10 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vote',
            name='value',
            field=models.SmallIntegerField(choices=[(1, 'UP'), (-1, 'DOWN')]),
        ),
    ]
