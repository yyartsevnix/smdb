from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import datetime
from core.models import Movie, Vote


class Command(BaseCommand):

    def handle(self, *args, **options):
        movie1 = Movie.objects.create(title='Great movie',
                             plot='Just a great movie',
                             year=2011, rating=0, runtime=100,
                             website='https://www.imdb.com/gm1')
        Movie.objects.create(title='Great movie 2',
                             plot='Just another great movie',
                             year=2012, rating=0, runtime=100,
                             website='https://www.imdb.com/gm2')
        Movie.objects.create(title='The Greatest movie',
                             plot='Just a greatest movie ever',
                             year=2013, rating=0, runtime=100,
                             website='https://www.imdb.com/ggm')

        user1 = User.objects.create_user("User Userovich", password="User1111")

        User.objects.create_user("User Userovsky", password="User1112")

        User.objects.create_user("User Userovenko", password="User1113")

        User.objects.create_user("User Userovjuk", password="User1114")

        Vote.objects.create(value=1, user=user1, movie=movie1,
                            voted_on=datetime.datetime.now())
